Insurance Hub issues:
1. I usually have trouble with background images in css. I'm sure you can fix that. Right now, the social media icons don't work, and the hero shot is an inline style element -- just to get it to work for now.
2. The correct font isn't loading.
3. The English Insurance Intake Form is in place and working. The Spanish one is in place, but not activated. Work your magic!
4. Crucial that we retain the file name for Insurance Plans (plans2.html) because a vanity URL depends on it. If you want to leave plans2.html and faqs2.html alone, I can fix any remaining issues once they are back in WCM. For example, the Insurance Intake Form button needs to be removed from both of those pages.