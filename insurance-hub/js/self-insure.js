
$(document).ready(function() {

    //stop carousel from auto rotating
    $('.carousel').carousel({
        interval: false
    }); 

    //Accordion Functionality
    $('.accordion-body').hide();
    
    $('.accordion-header').click(function() {

        if ($(this).next('.accordion-body').css('display') == 'block') {
            $('.accordion-body').slideUp('fast');
            $('.accord-indicator p').html('+').removeClass('adjust-icon');            
        } else {
            $('.accordion-body').slideUp('fast');
            $('.accord-indicator p').html('+').removeClass('adjust-icon');
            $(this).next('.accordion-body').slideDown('fast');
            $(this).find('.accord-indicator p').html('-').addClass('adjust-icon');
        }
    });

    //url sniffer for launching form

    //find prduct value from URL
    var currentURL = window.location.href;
    var splitURL = currentURL.split('form=');
    var emailURL = splitURL[1];
    //console.log(emailURL);

    if (emailURL == 'true') {
        $('#rfi').modal('show');
    } else {}

    //console.log('self insure script ran today');
});

    



